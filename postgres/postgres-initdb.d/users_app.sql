CREATE USER kszk_users;
CREATE DATABASE kszk_users;
ALTER USER kszk_users WITH PASSWORD 'kszk_users';
GRANT ALL PRIVILEGES ON DATABASE kszk_users TO kszk_users;
