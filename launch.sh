#!/bin/bash
# This script launches or updates the backend

echo " === Loading submodules === " 
git submodule init
git submodule update --remote

echo " === Building kszk-notebook === "
docker build kszk-notebook -t kszk-notebook

if ! (docker network ls | grep -q kszk-network); then
  echo " === Creating kszk-network === "
  docker network create kszk-network
fi

echo " === Launching docker-compose === "
docker-compose up -d --build
