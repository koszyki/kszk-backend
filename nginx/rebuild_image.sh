#!/usr/bin/env bash

docker-compose build --no-cache nginx
docker-compose kill nginx
docker-compose rm -f nginx
docker-compose up -d nginx

