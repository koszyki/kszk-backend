#!/bin/bash
# This script is a shorthand for communicating with users-cli.py

docker-compose exec users-app users-cli.py "$@"
