#!/usr/bin/env python3
# This script replaces environment variable placeholders in config
# with their actual values.
import os, re

with open('/etc/radicale/config') as file_:
    text = file_.read()

env_vars = re.findall(r"\${([A-Z_]+)}", text)
for env_var in env_vars:
    value = os.environ.get(env_var, "")
    text = text.replace("${"+env_var+"}", value)

with open('/etc/radicale/config', 'w') as file_:
    file_.write(text)
