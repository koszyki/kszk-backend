# -*- mode: conf -*-
# vim:ft=cfg

# Config file for Radicale - A simple calendar server
#
# Place it into /etc/radicale/config (global)
# or ~/.config/radicale/config (user)
#
# The current values are the default ones


[server]

# CalDAV server hostnames separated by a comma
# IPv4 syntax: address:port
# IPv6 syntax: [address]:port
# For example: 0.0.0.0:9999, [::]:9999
hosts = 0.0.0.0:5232

# Max parallel connections
max_connections = 20

# Max size of request body (bytes)
max_content_length = 100000000

# Socket timeout (seconds)
timeout = 30

# SSL flag, enable HTTPS protocol
#ssl = True

# SSL certificate path
#certificate = /etc/ssl/cert.pem

# SSL private key
#key = /etc/ssl/key.pem

# CA certificate for validating clients. This can be used to secure
# TCP traffic between Radicale and a reverse proxy
#certificate_authority =

# SSL Protocol used. See python's ssl module for available values
#protocol = PROTOCOL_TLSv1_2

# Available ciphers. See python's ssl module for available ciphers
#ciphers =

# Reverse DNS to resolve client address in logs
#dns_lookup = True


[encoding]

# Encoding for responding requests
request = utf-8

# Encoding for storing local collections
stock = utf-8

[auth]
type = radicale_auth_ldap

# LDAP server URL, with protocol and port
ldap_url = ldap://ldap:389

# LDAP base path
ldap_base = ${LDAP_ROOT_DN}

# LDAP login attribute
ldap_attribute = uid

# LDAP filter string
# placed as X in a query of the form (&(...)X)
# example: (objectCategory=Person)(objectClass=User)(memberOf=cn=calenderusers,ou=users,dc=example,dc=org)
ldap_filter = (objectClass=inetorgperson)

# LDAP dn for initial login, used if LDAP server does not allow anonymous searches
# Leave empty if searches are anonymous
ldap_binddn = cn=admin,${LDAP_ROOT_DN}

# LDAP password for initial login, used with ldap_binddn
ldap_password = ${LDAP_ADMIN_PASSWORD}

# LDAP scope of the search
ldap_scope = LEVEL

[rights]

# Rights backend
# Value: none | authenticated | owner_only | owner_write | from_file
#type = owner_only

# File for rights management from_file
#file = /etc/radicale/rights


[storage]

# Storage backend
# Value: multifilesystem
#type = multifilesystem

# Folder for storing local collections, created if not present
filesystem_folder = /data/collections

# Delete sync token that are older (seconds)
#max_sync_token_age = 2592000

# Command that is run after changes to storage
# Example: ([ -d .git ] || git init) && git add -A && (git diff --cached --quiet || git commit -m "Changes by "%(user)s)
#hook =


[web]

# Web interface backend
# Value: none | internal
#type = internal


[logging]

# Threshold for the logger
# Value: debug | info | warning | error | critical
#level = warning

# Don't include passwords in logs
#mask_passwords = True


[headers]

# Additional HTTP headers
Access-Control-Allow-Origin = *
Access-Control-Allow-Methods: GET, POST, OPTIONS, PROPFIND, PROPPATCH, REPORT, PUT, MOVE, DELETE, LOCK, UNLOCK
Access-Control-Allow-Headers: User-Agent, Authorization, Content-type, Depth, If-match, If-None-Match, Lock-Token, Timeout, Destination, Overwrite, Prefer, X-client, X-Requested-With
Access-Control-Expose-Headers: Etag, Preference-Applied
