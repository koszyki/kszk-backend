#!/usr/bin/env bash

source .env

docker-compose build --no-cache people-api-app
docker-compose kill people-api-app
docker-compose rm -f people-api-app
docker-compose up -d people-api-app

