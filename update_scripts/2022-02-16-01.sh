docker-compose up -d

check_if_postgres_is_running() {
    # run random psql command to see if it works at all
    docker-compose exec postgres psql -c "\d"
    if [ $? -ne 0 ]  ; then
        return 1
    else
        return 0
    fi
}

check_if_postgres_is_running
while [ $? -ne 0 ]  ; do
    check_if_postgres_is_running
done

docker-compose cp update_scripts/_2022-02-16-01-hedgedoc-postgres-rename.sql postgres:/update.sql
docker-compose exec postgres psql -f /update.sql

docker-compose down
