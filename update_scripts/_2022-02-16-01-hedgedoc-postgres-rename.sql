-- CREATE USER codimd;
-- CREATE DATABASE codimd;
-- ALTER USER codimd WITH PASSWORD 'codimd';
-- GRANT ALL PRIVILEGES ON DATABASE codimd TO codimd;
--
-- Rename all things codimd to hedgedoc
ALTER DATABASE codimd RENAME TO hedgedoc;
ALTER USER codimd RENAME TO hedgedoc;
ALTER USER hedgedoc WITH PASSWORD 'hedgedoc';
