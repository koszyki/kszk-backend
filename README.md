# kszk.eu backend
The goal of this repository is to make a stable and replicable backend setup.

## Deployment workflow
To deploy run:
```
./launch.sh
```

## User management
Users are managed via the `./users-cli.sh` script.
